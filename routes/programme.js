var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/acc', function(req, res, next) {
    res.render('acc', { title: 'Ateliers de création collective' });
});

router.get('/crea_pro', function(req, res, next) {
    res.render('crea_pro', { title: 'Créations professionnelles' });
});

router.get('/events', function(req, res, next) {
    res.render('events', { title: 'Evènements' });
});

router.get('/workshops', function(req, res, next) {
    res.render('workshops', { title: 'Workshops' });
});


module.exports = router;