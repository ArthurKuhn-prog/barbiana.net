var OpenedPanelInt = 0;
var CurrentMenu = null;

var archiveOpen = false;  

var classList = ["basicBox", "img", "closed", "openList"];
var lastOpened = "none";
var isClosing = false;
var nextIsPlain = false;

function getClass(classes) {
  for (var i = 0; i < classes.length; i++) {
    if (!classList.includes(classes[i])) {
      return classes[i];
    }
  }
}

function addRandomPos(objectList,newDiv) {
  let rand = getRandomInt(1,objectList.length);
  //Don't add divs between content and a content open button
  if (!objectList[rand-1].next().hasClass("openContentButton")) {
    console.log('Add after the following object');
    console.log(objectList[rand-1]);
    objectList[rand-1].after(newDiv);
  } else {
    console.log('Add after the content button');
    console.log(objectList[rand-1].next());
    objectList[rand-1].next().after(newDiv);
  }
}

function fillblanks(startBox = $("#main:first-child")) {

  startBox.nextAll('.fillBoxGrow').remove();

  //Basic variables assignement
  var divHTML = "<div class='fillBox '><div class='outlineBox'></div></div>";
  var plainDivHTML = "<div class='plainBox'></div>";

  var mainSel;
  if ($(".currentWrapper").length == 1) {
    if (archiveOpen) {
      mainSel = $(".currentWrapper , .archiveWrapper");
    } else {
      mainSel = $(".currentWrapper");
    }
  } else {
    mainSel = $("#main");
  }
  var mainOffset = mainSel.offset();
  var mainRightBorder = mainOffset.left + mainSel.width();
  var objectList = [];

  mainSel
    .children(".basicBox , .openContentButton, .dateBoxStarter , .fillBoxGrow")
    .each(function () {

      objectList.push($(this));
      var offset = $(this).offset();
      var width = $(this).width();
      var rightborder = offset.left + width;
      var rightborderWithMargin =
        rightborder + parseInt($(this).css("margin-right"));

      var nextElem = $(this).next();

      if($(this).hasClass("openContentButton") && $(this).hasClass("jmc")){
        console.log("debug");
      }

      if (
        nextElem !== undefined &&
        nextElem.length == 1 &&
        !nextElem.hasClass("archives")
      ) {
        while (nextElem.css("position") == "absolute") {
          nextElem = nextElem.next();
        }
        var nextElemOffset = nextElem.offset();
        let rightBorderDiff = Math.abs(Math.floor(rightborderWithMargin) - Math.floor(mainRightBorder));

        //If this element is last in his line (The next element has been wrapped and has not the same top offset)
        // && this element's right border is not aligned with his parent div
        if (
          Math.abs(nextElemOffset.top - offset.top) > 100 &&
          rightBorderDiff > 5
        ) {
          let newDiv = '';
          //If the space left is very low we just add a plain div
          if (rightBorderDiff < 60) {
            newDiv = plainDivHTML;
          } 
          //If the space is large we add a dotted border div of min-width 20
          else {
            if (nextIsPlain){
              newDiv = plainDivHTML;
            } else {
              newDiv = divHTML;
            }
            nextIsPlain = !nextIsPlain;
          }

          //We add a div box after a random element in its line to fix the space usage
          console.log(objectList);
          addRandomPos(objectList,newDiv);
          console.log("Right border diff : "+rightBorderDiff);
          //We clean the line array
          objectList = [];
        }
        if (
          Math.abs(nextElemOffset.top - offset.top) > 100
        ) {
          objectList = [];
        }
      } else {
        objectList = [];
      }
    });

    $('.fillBox , .plainBox').addClass("fillBoxGrow")
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function resetPanels() {
  //Reset Panels
  $(".FullMenu").removeClass("RightClose");
  $(".FullMenu").removeClass("LeftImgClose");
  $(".FullMenu").removeClass("FullOpen");
}

function resetMenuButtons() {
  $(".topMenu").find(".conteneurMenuLigne").removeClass("hover");
  $(".topMenu").find(".conteneurMenuLigne").removeClass("active");
  $(".topMenu").find(".smallArrow").text('§');
  $(".topMenu").removeClass("opacity1black");
  $(".topMenu").removeClass("fadeMenu");
}

function resetMenuOpacity() {
  $(".invisWrapper ").removeClass("visible");
}

function  resetContentFade(selector){
  if ($(document).width() > 1040) {
    selector.siblings().removeClass("opacity03");
    selector.removeClass("opacity03");
    selector.siblings(".dateBoxStarter").children().removeClass("opacity03");
  }
}

function  applyContentFade(selector, className){
  if ($(document).width() > 1040) {
    resetContentFade(selector);
    let currentDate = selector.prevAll(".dateBoxStarter, .dateBox").first();          //Fix for filrouge 2 boxes exception
    if (currentDate.hasClass("dateBoxStarter") && !currentDate.is(selector.prev()) && !selector.hasClass("filrouge")) {
      currentDate.children().not(".dateBox").addClass("opacity03");
    }
    selector.siblings().not(currentDate).not(selector.prev()).not("." + className + " , .titleBox, .archives , #archives , .fillBoxGrow , .synop , #workshop_intro").addClass("opacity03");
  }
}


$(document).ready(function () {

  //Stop a from triggering parent events
  $( "a" ).click(function( event ) {
    event.stopPropagation();
  });


  var lastObjectTouched = null;
  $(".infoBox").on('touchstart', function () {
    lastObjectTouched = $(this);
    console.log("touchstart");
  });
  $(document).bind('touchmove', function () {
      lastObjectTouched = null;
    console.log("touchmove");
  });
  $(document).bind('touchcancel', function () {
    lastObjectTouched = null;
    console.log("touchcancel");
  });
  $(".infoBox").not(".hovered").on('touchend', function () {

  console.log("touchend");

   if($(this).is(lastObjectTouched)) {
     //Either we open or close the infoBox
     if ($(this).hasClass("hovered")) {
       console.log("Showing infobox");
       $(this).removeClass("hovered");
     } else {
       console.log("Closing infobox");
       $(this).addClass("hovered");
     }

     lastObjectTouched = null;
   }

  });

  
    

 

  $.fn.animateRotate = function (angle, duration, easing, complete) {
    var args = $.speed(duration, easing, complete);
    var step = args.step;
    return this.each(function (i, e) {
      args.complete = $.proxy(args.complete, e);
      args.step = function (now) {
        $.style(e, "transform", "rotate(" + now + "deg)");
        if (step) return step.apply(e, arguments);
      };

      $({ deg: 0 }).animate({ deg: angle }, args);
    });
  };

  if ($(document).width() > 1040) {
    $(".basicBox.img:not(:has(video))").click(function () {
      let currentFS = $(this).find("img.imageBox").attr("src");
      $("#FSIMG").attr("src", currentFS);
      $(".fullscreenDIV").addClass("FSOPEN");
    });

    $(".fullscreenDIV").click(function () {
      if ($(".fullscreenDIV").hasClass("FSOPEN")) {
        $(".fullscreenDIV").removeClass("FSOPEN");
      }
    });
  }

  $(".LeftSide").hover(function () {
    if (OpenedPanelInt === 0) {
      console.log("left");

      //Reset panels
      resetPanels();

      //Show content
      $("#LeftImg").find(".invisWrapper").addClass("visible");

      //Close to mid and right panel to the right
      $("#MidImg").addClass("RightClose");
      $("#RightImg").addClass("RightClose");

      //Full Open Left and put it in front
      if (!$("#LeftImg").hasClass("FullOpen")) {
        $("#LeftImg").addClass("FullOpen");
      }
      $("#LeftImg").css("z-index", "101");
    }
  });

  $("#LeftImg").mouseleave(function () {
    if (OpenedPanelInt === 0) {
      console.log("leftleave");

      //Reset panels
      resetPanels();

      // Reset content opacity
      resetMenuOpacity();

      $("#LeftImg").css("z-index", "1");
    }
  });

  $(".Middle").hover(function () {
    if (OpenedPanelInt === 0) {
      console.log("mid");

      $("#MidImg").css("z-index", "101");

      //Reset panels
      resetPanels();

      //Show content
      $("#MidImg").find(".invisWrapper").addClass("visible");

      //Close to Left the left panel and right the right panel
      $("#LeftImg").addClass("LeftImgClose");
      $("#RightImg").addClass("RightClose");

      //Full Open mid and put it in front
      if (!$("#MidImg").hasClass("FullOpen")) {
        $("#MidImg").addClass("FullOpen");
      }
    }
  });

  $("#MidImg").mouseleave(function () {
    if (OpenedPanelInt === 0) {
      console.log("midleave");

      //Reset panels
      resetPanels();

      // Reset content opacity
      resetMenuOpacity();

      $("#MidImg").css("z-index", "1");
    }
  });

  $(".RightSide").hover(function () {
    if (OpenedPanelInt === 0) {
      console.log("right");

      //Reset panels
      resetPanels();

      //Close to Left the left and mid panel
      $("#LeftImg").addClass("LeftImgClose");
      $("#MidImg").addClass("LeftImgClose");

      //Show content
      $("#RightImg").find(".invisWrapper").addClass("visible");

      //Full Open Right and put it in front
      if (!$("#RightImg").hasClass("FullOpen")) {
        $("#RightImg").addClass("FullOpen");
      }

      $("#RightImg").css("z-index", "101");
    }
  });

  $("#RightImg").mouseleave(function () {
    if (OpenedPanelInt === 0) {
      console.log("leftleave");

      //Reset panels
      resetPanels();

      // Reset content opacity
      resetMenuOpacity();

      $("#RightImg").css("z-index", "1");
    }
  });

  $(".topMenu").hover(
    function () {
      if (!$(this).hasClass("opacity1black")) {
        $(this).addClass("opacity1black");
      }
      if (!$(this).find(".conteneurMenuLigne").hasClass("hover")) {
        $(this).find(".conteneurMenuLigne").addClass("hover");
      }
    },
    function () {
      if ($(this).hasClass("opacity1black")) {
        $(this).not(CurrentMenu).removeClass("opacity1black");
      }
      $(this).not(CurrentMenu).find(".conteneurMenuLigne").removeClass("hover");
    }
  );

  $(".menuOption").mouseenter(function () {
    $(this).find(".triangleRight").animateRotate(360, {
      easing: "swing",
    });
    $(this).find(".triangleRight").addClass("selected");
  });

  $(".menuOption").mouseleave(function () {
    $(this).find(".triangleRight").removeClass("selected");
  });

  //Left panel parallax
  $("#LeftImg").ready(function () {
    $(this).addClass("backgroundPara");
  });

  $(".topMenu").click(function () {
    let menuChosenId = $(this).attr("id");
    let menuChosenInt = 0;

    switch (menuChosenId) {
      case "leftmenubutton":
        menuChosenInt = 1;
        console.log("left menu button clicked");
        break;
      case "midmenubutton":
        menuChosenInt = 2;
        console.log("mid menu button clicked");
        break;
      case "rightmenubutton":
        menuChosenInt = 3;
        console.log("right menu button clicked");
        break;
    }

    //New menu is chosen
    if (menuChosenInt != OpenedPanelInt) {
      //We reset every menu button
      OpenedPanelInt = 0;
      resetMenuButtons();

      //Reset opacity
      resetMenuOpacity();

      //Highlight the menu we selected and fade the other ones
      $(".topMenu").not(this).addClass("fadeMenu");
      $(this).find(".conteneurMenuLigne").addClass("active");
      $(this).find(".smallArrow").text("▼");
      $(this).addClass("opacity1black");

      //Trigger opening event of the corresponding panel
      switch (menuChosenInt) {
        case 1:
          $(".LeftSide").mouseenter();
          break;
        case 2:
          $(".Middle").mouseenter();
          break;
        case 3:
          $(".RightSide").mouseenter();
          break;
      }

      //Set panel as the new fixed one
      OpenedPanelInt = menuChosenInt;
      CurrentMenu = this;
    } else {
      //Reset panel setup
      resetPanels();

      //Reset button look
      resetMenuButtons();

      //Reset opacity
      resetMenuOpacity();

      //If we already clicked on this before we reset everything
      OpenedPanelInt = 0;
      CurrentMenu = null;
    }
  });

  $(".backgroundImg").hover(
    function () {
      $(this).addClass("opacity0");
      $(this).next().addClass("opacity1");
    },
    function () {
      $(this).removeClass("opacity0");
      $(this).next().removeClass("opacity1");
    }
  );

  var mustClose;
  var timeOut;

  $(".menuLigne").hover(
    function () {
      mustClose = false;
      if(timeOut !== undefined){
        console.log(timeOut);
        clearTimeout(timeOut);
      }
      $(".hiddenMenu").addClass("showMenu");
      $("#flech").addClass("menuOpen");
    },
    function () {
      mustClose = true;
      timeOut = setTimeout(function () {
        if (mustClose) {
          $(".hiddenMenu").removeClass("showMenu");
          $("#flech").removeClass("menuOpen");
        }
      },1500);
    }
  );

  //Redirects workaround
  $(".START").click(function () {
    window.location.href = "https://www.start-invest.be";
  });
  $(".CTA").click(function () {
    window.location.href = "https://www.theatre-action.be/";
  });
  $(".FTA").click(function () {
    window.location.href = "https://www.federationtheatreaction.be/";
  });
  $(".FWB").click(function () {
    window.location.href = "http://www.federation-wallonie-bruxelles.be/";
  });
  $(".WALLO").click(function () {
    window.location.href = "https://spw.wallonie.be/";
  });
  $(".HAIN").click(function () {
    window.location.href = "https://portail.hainaut.be/";
  });
  $(".MNS").click(function () {
    window.location.href = "https://www.mons.be/";
  });
  $(".logo").click(function () {
    window.location.href = "../";
  });

  //Closable menu list

  var matchOpen = false;
  $(".openContentButton.match").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (matchOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".match.closed.openList").removeClass("openList");
      matchOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"match");
      $(".match.closed").addClass("openList");
      matchOpen = true;
    }
  });

  var delirOpen = false;
  $(".openContentButton.delir").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (delirOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".delir.closed.openList").removeClass("openList");
      delirOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"delir");
      $(".delir.closed").addClass("openList");
      delirOpen = true;
    }
  });

  var etvOpen = false;
  $(".openContentButton.etv").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (etvOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".etv.closed.openList").removeClass("openList");
      etvOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"etv");
      $(".etv.closed").addClass("openList");
      etvOpen = true;
    }
  });

  var actOpen = false;
  $(".openContentButton.act").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (actOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".act.closed.openList").removeClass("openList");
      actOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"act");
      $(".act.closed").addClass("openList");
      actOpen = true;
    }
  });

  var maOpen = false;
  $(".openContentButton.ma").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (maOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".ma.closed.openList").find("video").trigger("pause");
      $(".ma.closed.openList").removeClass("openList");

      maOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"ma");
      $(".ma.closed").addClass("openList");
      maOpen = true;
    }
  });

  var meOpen = false;
  $(".openContentButton.me").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (meOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".me.closed.openList").find("video").trigger("pause");
      $(".me.closed.openList").removeClass("openList");

      meOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"me");
      $(".me.closed").addClass("openList");
      meOpen = true;
    }
  });

  var pdfOpen = false;
  $(".openContentButton.pdf").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();

    if (pdfOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".pdf.closed.openList").find("video").trigger("pause");
      $(".pdf.closed.openList").removeClass("openList");

      pdfOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"pdf");
      $(".pdf.closed").addClass("openList");
      pdfOpen = true;
    }
  });

  var pilouOpen = false;
  $(".openContentButton.pilou").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (pilouOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".pilou.closed.openList").find("video").trigger("pause");
      $(".pilou.closed.openList").removeClass("openList");

      pilouOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"pilou");
      $(".pilou.closed").addClass("openList");
      pilouOpen = true;
    }
  });

  var amarOpen = false;
  $(".openContentButton.amarb").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (amarOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".amarb.closed.openList").find("video").trigger("pause");
      $(".amarb.closed.openList").removeClass("openList");

      amarOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"amarb");
      $(".amarb.closed").addClass("openList");
      amarOpen = true;
    }
  });

  var histOpen = false;
  $(".openContentButton.hist").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (histOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".hist.closed.openList").find("video").trigger("pause");
      $(".hist.closed.openList").removeClass("openList");

      histOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"hist");
      $(".hist.closed").addClass("openList");
      histOpen = true;
    }
  });

  var phOpen = false;
  $(".openContentButton.ph").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (phOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".ph.closed.openList").find("video").trigger("pause");
      $(".ph.closed.openList").removeClass("openList");

      phOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"ph");
      $(".ph.closed").addClass("openList");
      phOpen = true;
    }
  });

  var diseOpen = false;
  $(".openContentButton.dise").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (diseOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".dise.closed.openList").find("video").trigger("pause");
      $(".dise.closed.openList").removeClass("openList");

      diseOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"dise");
      $(".dise.closed").addClass("openList");
      diseOpen = true;
    }
  });

  var zinebOpen = false;
  $(".openContentButton.zineb").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (zinebOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".zineb.closed.openList").find("video").trigger("pause");
      $(".zineb.closed.openList").removeClass("openList");

      zinebOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"zineb");
      $(".zineb.closed").addClass("openList");
      zinebOpen = true;
    }
  });

  var colabOpen = false;
  $(".openContentButton.col").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (colabOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".col.closed.openList").find("video").trigger("pause");
      $(".col.closed.openList").removeClass("openList");

      colabOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"col");
      $(".col.closed").addClass("openList");
      colabOpen = true;
    }
  });

  var joliOpen = false;
  $(".openContentButton.joli").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (joliOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".joli.closed.openList").find("video").trigger("pause");
      $(".joli.closed.openList").removeClass("openList");

      joliOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"joli");
      $(".joli.closed").addClass("openList");
      joliOpen = true;
    }
  });

  var maskOpen = false;
  $(".openContentButton.mask").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();

    if (maskOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".mask.closed.openList").find("video").trigger("pause");
      $(".mask.closed.openList").removeClass("openList");

      maskOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"mask");
      $(".mask.closed").addClass("openList");
      maskOpen = true;
    }
  });

  var croisOpen = false;
  $(".openContentButton.crois").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (croisOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".crois.closed.openList").find("video").trigger("pause");
      $(".crois.closed.openList").removeClass("openList");

      croisOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"crois");
      $(".crois.closed").addClass("openList");
      croisOpen = true;
    }
  });

  var filrougeOpen = false;
  $(".openContentButton.filrouge").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (filrougeOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".filrouge.closed.openList").find("video").trigger("pause");
      $(".filrouge.closed.openList").removeClass("openList");

      filrougeOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"filrouge");
      $(".filrouge.closed").addClass("openList");
      filrougeOpen = true;
    }
  });

  var jmcOpen = false;
  $(".openContentButton.jmc").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (jmcOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".jmc.closed.openList").find("video").trigger("pause");
      $(".jmc.closed.openList").removeClass("openList");

      jmcOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"jmc");
      $(".jmc.closed").addClass("openList");
      jmcOpen = true;
    }
  });

  var fittaOpen = false;
  $(".openContentButton.fitta").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (fittaOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".fitta.closed.openList").find("video").trigger("pause");
      $(".fitta.closed.openList").removeClass("openList");

      fittaOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"fitta");
      $(".fitta.closed").addClass("openList");
      fittaOpen = true;
    }
  });

  var vitrineOpen = false;
  $(".openContentButton.vitrine").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (vitrineOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".vitrine.closed.openList").find("video").trigger("pause");
      $(".vitrine.closed.openList").removeClass("openList");

      vitrineOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"vitrine");
      $(".vitrine.closed").addClass("openList");
      vitrineOpen = true;
    }
  });

  var furtifOpen = false;
  $(".openContentButton.furtif").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (furtifOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".furtif.closed.openList").find("video").trigger("pause");
      $(".furtif.closed.openList").removeClass("openList");

      furtifOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"furtif");
      $(".furtif.closed").addClass("openList");
      furtifOpen = true;
    }
  });

  var masquedOpen = false;
  $(".openContentButton.masqued").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (masquedOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".masqued.closed.openList").find("video").trigger("pause");
      $(".masqued.closed.openList").removeClass("openList");

      masquedOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";

      console.log($(this).siblings());
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"masqued");
      $(".masqued.closed").addClass("openList");
      masquedOpen = true;
    }
  });

  var impossFamilleOpen = false;
  $(".openContentButton.impossFamille").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (impossFamilleOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".impossFamille.closed.openList").find("video").trigger("pause");
      $(".impossFamille.closed.openList").removeClass("openList");

      impossFamilleOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";

      console.log($(this).siblings());
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"impossFamille");
      $(".impossFamille.closed").addClass("openList");
      impossFamilleOpen = true;
    }
  });

  var jeunesCreaOpen = false;
  $(".openContentButton.jeunesCrea").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (jeunesCreaOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".jeunesCrea.closed.openList").find("video").trigger("pause");
      $(".jeunesCrea.closed.openList").removeClass("openList");

      jeunesCreaOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";

      console.log($(this).siblings());
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"jeunesCrea");
      $(".jeunesCrea.closed").addClass("openList");
      jeunesCreaOpen = true;
    }
  });

    var identitaCreaOpen = false;
  $(".openContentButton.identitaCrea").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (identitaCreaOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".identitaCrea.closed.openList").find("video").trigger("pause");
      $(".identitaCrea.closed.openList").removeClass("openList");

      identitaCreaOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";

      console.log($(this).siblings());
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"identitaCrea");
      $(".identitaCrea.closed").addClass("openList");
      identitaCreaOpen = true;
    }
  });

  var filrougeVidzOpen = false;
  $(".openContentButton.filrougeVidz").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (filrougeVidzOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".filrougeVidz.closed.openList").find("video").trigger("pause");
      $(".filrougeVidz.closed.openList").removeClass("openList");

      filrougeVidzOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";

      console.log($(this).siblings());
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"filrougeVidz");
      $(".filrougeVidz.closed").addClass("openList");
      filrougeVidzOpen = true;
    }
  });

  var bozzaOpen = false;
  $(".openContentButton.bozza").click(function () {
    $(this).nextAll(".fillBox , .plainBox").remove();
    if (bozzaOpen) {
      //Windows are opened
      console.log("closing");
      lastOpened = "none";
resetContentFade($(this));
      $(this).find(".arrow , .minus").removeClass("opened");
      $(".bozza.closed.openList").find("video").trigger("pause");
      $(".bozza.closed.openList").removeClass("openList");

      bozzaOpen = false;
    } else {
      //Windows are closed
      console.log("opening");
      lastOpened = "none";

      console.log($(this).siblings());
      $(this).find(".arrow , .minus").addClass("opened");
      applyContentFade($(this),"bozza");
      $(".bozza.closed").addClass("openList");
      bozzaOpen = true;
    }
  });
  

  $(".infoBox , .openContentButton")
    .not("#synop")
    .not("#archives")
    .each(function (index) {
      let col = randomColor({ hue: "#264653", luminosity: "dark" });
      $(this).css("background-color", col);
    });
});

var isChanged = false;

$(window).on('load',function(){

  if ($(document).width() > 1040) {
    fillblanks();
  }

  $(window).resize(function () {
  $(".fillBoxGrow").remove();
  if ($(document).width() > 1040) {
    isChanged = true;
    setTimeout(function(){
      if(isChanged) {
        isChanged = !isChanged;
        fillblanks();
      }
    },800);
  }
});

$(".closed").on(
  "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",
  function (event) {
    if ($(document).width() > 1040) {
      if (event.originalEvent.propertyName !== "margin-right") return;
      let thisClass = getClass($(this).attr("class").split(/\s+/));

      if (thisClass != lastOpened || isClosing) {
        lastOpened = thisClass;
        isClosing = false;
        fillblanks($(this));
        console.log("Fillingblanks");
      }
    }
  }
);

  var archiveH = $(".archiveWrapper").height();
  var archive = $(".archiveWrapper");
  archive.css("transform", "translateY(" + -archiveH + "px)");
  archive.css("max-height", 0);

  console.log(archiveH);
  var locked = false;

  $(".archives").click(function () {
    console.log("lock state : " + locked);
    $(".archiveWrapper").not(".easeInOut").addClass("easeInOut");
    if (!locked) {
      if (archiveOpen) {
        archiveH = $(".archiveWrapper").height();

        locked = true;
        console.log("Closing height = "+archiveH);
        archive.css("transform", "translateY(" + -archiveH + "px)");
        archive.css("max-height", 0);

        archiveOpen = false;
      } else {
        locked = true;
        archive.css("transform", "translateY(" + 0 + ")");
        console.log("Opening height = "+archiveH);
        if ($(window).width() > 1040) {
          archive.css("max-height", 4000);
        } else {
          archive.css("max-height", 15000);
        }
        archiveOpen = true;
      }
    }
  });

  $(".archiveWrapper").on(
      "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",
      function () {
        //Free up lock
        locked = false;
      }
  );
});
